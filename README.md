## Requirements
> Node.js v8.x with `async/await`
>
> This version of  **hapi v17**. 

**Requirements**

- Node.js **v8.x** or later
- NPM/Yarn to install the project’s dependencies
- [Vagrant](https://www.vagrantup.com/) or a [MongoDB](https://docs.mongodb.com/manual/installation/) instance on your machine



## Setup and Run


# install dependencies
npm i

# start the Vagrant box
vagrant up

# create your secrets.env file from secrets.env.example
cp secrets.env.sample secrets.env

# import sample data
npm run pumpitup

# start the server
node server.js

# that’s it :)
```


