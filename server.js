'use strict'

const Hapi = require('hapi')
const Path = require('path')
const Dotenv = require('dotenv')
const Laabr = require('laabr')

// import environment variables from local secrets.env file
Dotenv.config({ path: Path.resolve(__dirname, 'secrets.env') })

// create new server instance and connection information
const server = new Hapi.Server({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || '8010',
  routes: {
    cors: {
      // change this for production
      origin: ['*']
    }
  }
})

// register plugins, configure views and start the server instance
async function start () {
  // register plugins to server instance
  await server.register([
    {
      // better error handling during development
      plugin: require('hapi-dev-errors'),
      options: {
        showErrors: process.env.NODE_ENV !== 'production',
        useYouch: true
        // template: 'my-error-template'
      }
    },
    // {
    //   // token secure your api
    //   plugin: require('crumb'),
    //   options: {
    //     key: 'keepMeSafeMyBonFromCsrf',
    //     cookieOptions: {
    //       isSecure: process.env.NODE_ENV === 'production'
    //     }
    //   }
    // },
    {
      // for logging (laabr)
      plugin: Laabr.plugin,
      options: {
        colored: true,
        hapiPino: {
          logPayload: false
        }
      }
    },
    {
      plugin: require('./api/authentication')
    },
    {
      plugin: require('./api/users')
    },
    {
      plugin: require('./api/movies')
    }
  ])

  // start your server
  try {
    await server.start()
    console.log(`Server started → ${server.info.uri}`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

start()
