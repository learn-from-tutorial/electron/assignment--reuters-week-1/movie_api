'use strict'

const Boom = require('boom')
const Joi = require('joi')
const Path = require('path')
const Movie = require(Path.resolve(__dirname, '..', '..', 'models')).Movie

const Handler = {
  index: {
    // auth: 'jwt',
    handler: async (request, h) => {
      const movies = await Movie.find()

      if (!movies.length) {
        throw Boom.notFound('Movie not found!')
      }
      return movies
    }
  },

  show: {
    handler: async (request, h) => {
      const id = request.params.id

      const movie = Movie.findOne({
        _id: id
      })

      if (!movie) {
        throw Boom.notFound('Movie not found!')
      }
      return movie
    }
  },

  create: {
    handler: async (request, h) => {
      let movie = new Movie()

      movie.title = request.payload.title
      movie.images = request.payload.images

      try {
        await movie.save()
        return h.response(movie).code(201)
      } catch (error) {
        throw Boom.badRequest(error)
      }
    }
  },

  edit: {
    handler: async (request, h) => {
      // TODO
    }
  },

  destroy: {
    handler: async (request, h) => {
      // TODO
    }
  }
}

module.exports = Handler
