'use strict'

const Handler = require('./handler')

const Routes = [
  {
    method: 'GET',
    path: '/api/movies',
    config: Handler.index
  },
  {
    method: 'GET',
    path: '/api/movies/{id}',
    config: Handler.show
  },
  {
    method: 'POST',
    path: '/api/movies',
    config: Handler.create
  },
  {
    method: 'PUT',
    path: '/api/movies',
    config: Handler.edit
  },
  {
    method: 'DELETE',
    path: '/api/movies',
    config: Handler.destroy
  }
]

module.exports = Routes
