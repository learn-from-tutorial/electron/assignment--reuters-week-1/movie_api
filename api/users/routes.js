'use strict'

const Handler = require('./handler')

const Routes = [
  {
    method: 'GET',
    path: '/api/users',
    config: Handler.index
  },
  {
    method: 'GET',
    path: '/api/users/{id}',
    config: Handler.show
  },
  {
    method: 'PUT',
    path: '/api/users',
    config: Handler.edit
  },
  {
    method: 'DELETE',
    path: '/api/users',
    config: Handler.destroy
  },
  {
    method: 'POST',
    path: '/api/users',
    config: Handler.signup
  },
  {
    method: 'POST',
    path: '/api/users/authenticate',
    config: Handler.login
  },
  {
    method: 'GET',
    path: '/api/auth/refresh',
    config: Handler.refreshToken
  }
]

module.exports = Routes
