'use strict'

const Boom = require('boom')
const Joi = require('joi')
const Path = require('path')
const JWT = require('jsonwebtoken')
const ErrorExtractor = require('../../utils/error-extractor')

const User = require(Path.resolve(__dirname, '..', '..', 'models')).User

const Handler = {
  signup: {
    handler: async (request, h) => {
      // implement the actual sign up processing
      // if (request.auth.isAuthenticated) {
      //   return h.redirect('/profile')
      // }

      // shortcut
      const payload = request.payload

      try {
        // check whether the email address is already registered
        let user = await User.findByEmail(payload.email)

        if (user) {
          // create an error object that matches our error structure
          const message = 'Email address is already registered'
          throw new Boom(message, {
            statusCode: 409,
            data: { email: { message } }
          })
        }

        // create a new user
        const newUser = new User({
          email: payload.email,
          password: payload.password,
          scope: ['user']
        })

        // don’t store the plain password in your DB, hash it!
        user = await newUser.hashPassword()
        user = await user.save()

        request.cookieAuth.set({ id: user.id })

        // const discoverURL = `http://${request.headers.host}/discover`
        // Mailer.fireAndForget('welcome', user, '📺 Futureflix — Great to see you!', { discoverURL })

        // \o/ wohoo, sign up successful
        return h.response('signup-success').code(201)
      } catch (err) {
        const status = err.isBoom ? err.output.statusCode : 400

        return h
          .response({
            email: payload.email,
            errors: err.data
          })
          .code(status)
      }
    },
    validate: {
      options: {
        stripUnknown: true,
        abortEarly: false
      },
      payload: {
        email: Joi.string()
          .email({ minDomainAtoms: 2 })
          .required()
          .label('Email address'),
        password: Joi.string()
          .min(6)
          .required()
          .label('Password')
      },
      failAction: (request, h, error) => {
        // prepare formatted error object
        const errors = ErrorExtractor(error)
        // remember the user’s email address and pre-fill for comfort reasons
        const email = request.payload.email

        return h
          .view('signup', {
            email,
            errors
          })
          .code(400)
          .takeover()
      }
    }
  },

  login: {
    auth: 'basic',
    handler: async ({ user }) => {
      const authToken = JWT.sign(user.toObject(), process.env.JWT_SECRET_KEY, {
        algorithm: 'HS256',
        expiresIn: '15m'
      })

      const refreshToken = await user.createRefreshToken()

      return { authToken, refreshToken }
    },
    validate: {
      options: {
        stripUnknown: true,
        abortEarly: false
      },
      payload: {
        email: Joi.string()
          .email({ minDomainAtoms: 2 })
          .required()
          .label('Email address'),
        password: Joi.string()
          .min(6)
          .required()
          .label('Password')
      },
      failAction: async (request, h, error) => {
        // prepare formatted error object
        const errors = ErrorExtractor(error)
        // remember the user’s email address and pre-fill for comfort reasons
        const email = request.payload.email

        return h
          .view('login', {
            email,
            errors
          })
          .code(400)
          .takeover()
      }
    }
  },

  refreshToken: {
    auth: false,
    handler: async request => {
      const user = await User.findByRefreshToken(request.query.refreshToken)

      if (!user) {
        throw Boom.unauthorized('Invalid refresh token')
      }

      const authToken = JWT.sign(user.toObject(), process.env.JWT_SECRET_KEY, {
        algorithm: 'HS256',
        expiresIn: '1h'
      })

      const refreshToken = await user.createRefreshToken()

      return { authToken, refreshToken }
    },
    validate: {
      query: {
        refreshToken: Joi.string()
          .label('Refresh token')
          .trim()
          .required()
      }
    }
  },

  // logout: {
  //   auth: 'session',
  //   handler: (request, h) => {
  //     request.cookieAuth.clear()
  //     return h.redirect('/')
  //   }
  // }

  index: {
    handler: async (request, h) => {
      // TODO
    }
  },

  show: {
    handler: async (request, h) => {
      // TODO
    }
  },

  edit: {
    handler: async (request, h) => {
      // TODO
    }
  },

  destroy: {
    handler: async (request, h) => {
      // TODO
    }
  }
}

module.exports = Handler
