'use strict'

const Mongoose = require('mongoose')
const Schema = Mongoose.Schema
const Bcrypt = require('bcrypt')
const Boom = require('boom')
const Crypto = require('crypto')
// const When = require('when')

const SALT_WORK_FACTOR = 12

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    trim: true,
    required: true
  },
  username: {
    type: String,
    unique: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  },
  refreshToken: {
    type: String,
    trim: true,
    unique: true, // creates a MongoDB index, ensuring unique values
    sparse: true // this makes sure the unique index applies to not null values only (= unique if not null)
  },
  passwordResetDeadline: Date,
  scope: [String]
}

// {
//   // minimize JSON for API: remove _id, __v properties
//   toJSON: {
//     virtuals: true,
//     transform: function (doc, ret, options) {
//       delete ret._id
//       // delete ret.__v  // removed by "versionKey" option
//       // delete ret.deletedAt
//       delete ret.password
//       return ret
//     },
//     versionKey: false // removes __v from JSON response
//   },
//   toObject: { virtuals: true }
// }
)

/**
 * Model Methods (Statics)
 */
userSchema.statics.findByEmail = function (email) {
  return this.findOne({ email })
}

/**
 * Instance Methods
 */
userSchema.methods.hashPassword = async function () {
  const salt = await Bcrypt.genSalt(SALT_WORK_FACTOR)
  const hash = await Bcrypt.hash(this.password, salt)

  this.password = hash
  return this
}

// this is the new method
userSchema.methods.comparePassword = async function (candidatePassword) {
  const isMatch = await Bcrypt.compare(candidatePassword, this.password)

  if (isMatch) {
    return this
  }

  const message = 'The entered password is incorrect'
  throw new Boom(message, {
    statusCode: 400,
    data: { password: { message } }
  })
}

// Create a Refresh Token
userSchema.methods.createRefreshToken = async function () {
  const refreshToken = Crypto.randomBytes(30).toString('hex')

  this.refreshToken = Crypto.createHash('sha512')
    .update(refreshToken)
    .digest('hex')

  await this.save()

  return refreshToken
}

// Find a User by Refresh Token
userSchema.statics.findByRefreshToken = function (refreshToken) {
  return this.findOne({
    refreshToken: Crypto.createHash('sha512')
      .update(refreshToken)
      .digest('hex')
  })
}

module.exports = Mongoose.model('User', userSchema)
