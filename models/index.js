'use strict'

const Mongoose = require('mongoose')
// import once in your project
const User = require('./User')
const Movie = require('./Movie')


// tell Mongoose to use Node.js promises
Mongoose.Promise = global.Promise

const options = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  user: process.env.DATABASE_USERNAME,
  pass: process.env.DATABASE_PASSWORD
}
const dbUrl = process.env.DATABASE || 'mongodb://localhost:27017/electron-movie'
// Connect to your database
Mongoose.connect(dbUrl, options)

// listen for connection errors and print the message
Mongoose.connection.on('error', err => {
  console.error(`⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨 ⚡️ 🚨  → ${err.message}`)
  throw err
})

// use ES6 shorthands: "propertyName: variableName" equals "propertyName"
module.exports = {
  User,
  Movie
}
