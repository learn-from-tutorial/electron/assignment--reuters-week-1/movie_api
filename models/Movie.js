'use strict'

const Mongoose = require('mongoose')
const MongooseRandom = require('mongoose-simple-random')
const Schema = Mongoose.Schema

const movieSchema = new Schema({
  title: {
    type: String,
    unique: true,
    trim: true,
    required: true
  },
  images: {
    poster: String,
    background: String
  },
  year: Number,
  tagline: String,
  overview: String,
  released: String,
  runtime: Number, // in minutes
  trailer: String,
  homepage: String,
  rating: Number,
  votes: Number,
  genres: [String],
  language: String,
  certification: String
},

{
  // minimize JSON for API: remove _id, __v properties
  toJSON: {
    virtuals: true,
    transform: function (doc, ret, options) {
      delete ret._id
      // delete ret.__v  // removed by "versionKey" option
      delete ret.deletedAt
      return ret
    },
    versionKey: false // removes __v from JSON response
  },
  toObject: { virtuals: true }
})

module.exports = Mongoose.model('Movie', movieSchema)
